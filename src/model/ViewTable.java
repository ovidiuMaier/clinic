package model;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class ViewTable {
	
	 public static DefaultTableModel patientsToTableModel(ArrayList<Patient> patients) {
	        Vector<String> columnNames = new Vector<String>();
	        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
	        columnNames.add("ID");
	        columnNames.add("Name");
	        columnNames.add("CNP");
	        columnNames.add("Date of Birth");
	        columnNames.add("Address");

	        for (int i = 0; i < patients.size(); i++) {
	        	Patient b = patients.get(i);
	            Vector<Object> row = new Vector<Object>(); 
	            
	            row.add(b.getId());
	            row.add(b.getName());
	            row.add(b.getCNP());
	            row.add(b.getBirth());
	            row.add(b.getAddress());
	            
	            rows.add(row);
	        }

	        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

	            public boolean isCellEditable(int row, int column) {
	                return false;
	            }
	        };
	        return model;
	    }
	 
	 public static DefaultTableModel consultationsToTableModel(ArrayList<Consultation> cons) {
	        Vector<String> columnNames = new Vector<String>();
	        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
	        columnNames.add("ID");
	        columnNames.add("Patient ID");
	        columnNames.add("Doctor ID");
	        columnNames.add("Date");

	        for (int i = 0; i < cons.size(); i++) {
	            Consultation c = cons.get(i);
	            Vector<Object> row = new Vector<Object>(); 
	            row.add(c.getId());
	            row.add(c.getPatientId());
	            row.add(c.getDoctorId());
	            row.add(c.getDate());
	            rows.add(row);
	        }

	        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

	            public boolean isCellEditable(int row, int column) {
	                return false;
	            }
	        };
	        return model;
	    }
	 	 
	 public static DefaultTableModel usersToTableModel(ArrayList<User> users) {
	        Vector<String> columnNames = new Vector<String>();
	        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
	        columnNames.add("ID");
	        columnNames.add("Email");
	        columnNames.add("Password");
	        columnNames.add("Role");

	        for (int i = 0; i < users.size(); i++) {
	            User u = users.get(i);
	            Vector<Object> row = new Vector<Object>(); 
	            row.add(u.getId());
	            row.add(u.getUsername());
	            row.add(u.getPassword());
	            row.add(u.getRole());
	            rows.add(row);
	        }

	        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

	            public boolean isCellEditable(int row, int column) {
	                return false;
	            }
	        };
	        return model;
	    }

}

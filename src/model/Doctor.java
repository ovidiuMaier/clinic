package model;

public class Doctor extends User{
	
	private int doctor_id;
	private String email;
	private String password;


	public Doctor(String email, String password) {
		super(email, password);
	}
	

	public int getdoctor_id() {
		return doctor_id;
	}

	public void setdoctor_id(int doctor_id) {
		this.doctor_id = doctor_id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	

}

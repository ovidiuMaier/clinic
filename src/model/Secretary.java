package model;

import java.io.Serializable;

public class Secretary extends User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Secretary(String email, String password) {
		super(email, password);
	}
	
	private int secretary_id;
	private String email;
	private String password;

	public int getsecretary_id() {
		return secretary_id;
	}

	public void setsecretary_id(int secretary_id) {
		this.secretary_id = secretary_id;
	}

	public String getUser() {
		return email;
	}

	public void setUser(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Secretary [secretary_id=" + secretary_id + ", email=" + email + ", password=" + password + "]";
	}
	

}

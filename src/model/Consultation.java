package model;

public class Consultation {

	private int id;
	private int patient_id;
	private int doctor_id;
	private String date;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPatientId() {
		return patient_id;
	}
	public void setPatientId(int patient_id) {
		this.patient_id = patient_id;
	}
	public int getDoctorId() {
		return doctor_id;
	}
	public void setDoctorId(int doctor_id) {
		this.doctor_id = doctor_id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "Consultation [id=" + id + ", patient_id=" + patient_id + ", doctor_id=" + doctor_id + ", date=" + date + "]";
	}
	
	
}

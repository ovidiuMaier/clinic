package model;

import java.io.Serializable;
import java.util.Observable;

public class Patient  implements Serializable {
	
	private int id;
	private String name;
	private int CNP;
	private String birth;
	private String address;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCNP() {
		return CNP;
	}
	public void setCNP(int CNP) {
		this.CNP = CNP;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String toString() {
		return "Patient [id=" + id + ", name=" + name + ", CNP=" + CNP + ", birth=" + birth + ", address=" + address + "]";
	}
	
	
}

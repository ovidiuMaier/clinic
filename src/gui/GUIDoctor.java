package gui;
import java.awt.BorderLayout;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.*;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class GUIDoctor extends JFrame implements Observer {

	private JPanel contentPane;
	private JTable consultationTable;
	private JTextField dateField;
	private JTextField iddField;
	private JTextField idpField;
	private JTextField idField;
	private JButton btnAdd;
	private Watched model= Watched.getInstance();
	
	public void update(Observable arg0, Object arg1) {
		JOptionPane.showMessageDialog(null, "Your patient has arrived!");
		System.out.println("Doctor notified of arrival");
	}

	public GUIDoctor() {
		System.out.println(" doctor MODEL" + model);
		model.addObserver(this);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Doctor area");
		setBounds(100, 100, 700, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 525, 175);
		contentPane.add(scrollPane);
		
		consultationTable = new JTable();
		scrollPane.setViewportView(consultationTable);
		
		JLabel lblId_1 = new JLabel("ID");
		lblId_1.setBounds(10, 470, 66, 14);
		contentPane.add(lblId_1);
		
		JLabel lblPatient = new JLabel("Patient");
		lblPatient.setBounds(10, 495, 66, 14);
		contentPane.add(lblPatient);
		
		JLabel lblDoctor = new JLabel("Doctor");
		lblDoctor.setBounds(10, 520, 66, 14);
		contentPane.add(lblDoctor);
	
		JLabel lblSchedule = new JLabel("Date");
		lblSchedule.setBounds(10, 545, 66, 14);
		contentPane.add(lblSchedule);
		
		idField = new JTextField();
		idField.setBounds(59, 470, 110, 20);
		contentPane.add(idField);
		idField.setColumns(10);
		
		idpField = new JTextField();
		idpField.setBounds(59, 495, 110, 20);
		contentPane.add(idpField);
		idpField.setColumns(10);
		
		iddField = new JTextField();
		iddField.setBounds(59, 520, 110, 20);
		contentPane.add(iddField);
		iddField.setColumns(10);
		
		dateField = new JTextField();
		dateField.setBounds(59, 545, 110, 20);
		contentPane.add(dateField);
		dateField.setColumns(10);
		
		
		btnAdd = new JButton("Create");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 9998); 
				Consultation c=new Consultation();
				c.setId(Integer.parseInt(idField.getText()));
				c.setPatientId(Integer.parseInt(idpField.getText()));
				c.setDoctorId(Integer.parseInt(iddField.getText()));
				c.setDate(dateField.getText());
				client.addConsultation(c);
				try {
					client.connect();
					client.writeMessage("addConsultation:"+idField.getText()+":"+idpField.getText()+":"
						+iddField.getText()+":"+dateField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!" + e1.getMessage());
				}
				
				init();
			}
		});
		btnAdd.setBounds(197, 545, 89, 23);
		contentPane.add(btnAdd);
		init();
	}
	
	private void init()
	{
		Client client = new Client("localhost", 9998);
		
		try {
			client.connect();
			client.writeMessage("getConsultationList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!" + e.getMessage());
		}
		consultationTable.setModel(ViewTable.consultationsToTableModel(client.getConsultations()));
		
	}
}

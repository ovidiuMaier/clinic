package gui;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Observable;

import model.*;

public class Client {
	private String hostname;
	private int port;
	Socket socketClient;
	
	private ArrayList<Consultation> consultationList= new ArrayList<Consultation>();
	private ArrayList<User> userList= new ArrayList<User>();
	private ArrayList<Patient> patientList= new ArrayList<Patient>();
	
	public ArrayList<Patient> getPatients(){
		return patientList;
	}
	
	public ArrayList<Consultation> getConsultations(){
		return consultationList;
	}
	
	public ArrayList<User> getUsers(){
		return userList;
	}
	
	public boolean checkAvailable(String date,int idD){
		for(int i=0;i<consultationList.size();i++)
		{
			System.out.println("check av:"+consultationList.get(i).getDoctorId()+" "+idD +"&"+ consultationList.get(i).getDate()+" "+date);
			if(consultationList.get(i).getDoctorId()==idD && consultationList.get(i).getDate().equals(date))
				{
				System.out.println("Doctor not available");
				return false;
				}
		}
		return true;
	}
	
	public Client(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;
	}

	public void connect() throws UnknownHostException, IOException {
		System.out.println("Attempting to connect to " + hostname + ":" + port);
		socketClient = new Socket(hostname, port);
		System.out.println("Connected");
	}

	public void readResponse() throws IOException {		
		String delims = ",";		
		
		String userInput;
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
		System.out.println("Server response:");
		
		while ((userInput = stdIn.readLine()) != null) {
			System.out.println(userInput);			
			String[] tokens = userInput.split(delims);
			userInput=tokens[0];
			
			switch(userInput)
			{case "validSecretary":
				GUISecretary sgui=new GUISecretary();
				sgui.setVisible(true);
				break;
			case "validDoctor":
				GUIDoctor dgui=new GUIDoctor();
				dgui.setVisible(true);
				break;
			case "validAdmin":
				GUIAdmin agui=new GUIAdmin();
				agui.setVisible(true);
				break;
			case "patientList":				
				for(int i=1;i<tokens.length;i++)
				{
					
					Patient p =new Patient();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					p.setId(Integer.parseInt(tok[0]));
					p.setName(tok[1]);
					p.setCNP(Integer.parseInt(tok[2]));
					p.setBirth(tok[3]);
					p.setAddress(tok[4]);
					
					patientList.add(p);	
				}
				break;
			case "consultationList":				
				for(int i=1;i<tokens.length;i++)
				{
					Consultation c =new Consultation();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					c.setId(Integer.parseInt(tok[0]));
					c.setPatientId(Integer.parseInt(tok[1]));
					c.setDoctorId(Integer.parseInt(tok[2]));
					c.setDate(tok[3]);
					
					consultationList.add(c);	
				}
				break;
			case "userList":				
				for(int i=1;i<tokens.length;i++)
				{					
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
					User u =new User(tok[1],tok[2]);		
					u.setId(Integer.parseInt(tok[0]));
					u.setRole(tok[3]);				
					
					userList.add(u);
				}
				break;
			case "patientArrived":
				Watched p=Watched.getInstance();
				p.arrived();
				break;
			}
		}
	}

	public void closeConnection() throws IOException {
		socketClient.close();
	}

	public void writeMessage(String message) throws IOException {
		String process = message + (char) 13;
		BufferedWriter stdOut = new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));
		stdOut.write(process);
		stdOut.flush();
	}

	public void addPatient(Patient p) {
		patientList.add(p);		
	}

	public void updatePatient(Patient p) {
		for(int i=0;i<patientList.size();i++)
			if(p.getId() == patientList.get(i).getId())
			{
				patientList.get(i).setName(p.getName());
				patientList.get(i).setCNP(p.getCNP());
				patientList.get(i).setBirth(p.getBirth());
				patientList.get(i).setAddress(p.getAddress());
			}
		
	}

	public void addConsultation(Consultation c) {
		consultationList.add(c);		
	}

	public void updateConsultation(Consultation c) {
		for(int i=0;i<consultationList.size();i++)
			if(c.getId() == consultationList.get(i).getId())
			{
				consultationList.get(i).setPatientId(c.getPatientId());
				consultationList.get(i).setDoctorId(c.getDoctorId());
				consultationList.get(i).setDate(c.getDate());
			}
		
	}

	public void deleteConsultation(int id) {
		ArrayList<Consultation> newList=new ArrayList<Consultation>();
		for(int i=0;i<consultationList.size();i++)
			if(id != consultationList.get(i).getId())
				newList.add(consultationList.get(i));
		
		consultationList=newList;
		
	}

	public void addUser(User u) {
		userList.add(u);		
	}

	public void updateUser(User u) {
		for(int i=0;i<userList.size();i++)
			if(u.getId() == userList.get(i).getId() && u.getRole()==userList.get(i).getRole())
			{
				userList.get(i).setUsername(u.getUsername());
				userList.get(i).setPassword(u.getPassword());
				
			}
	}

	public void deleteUser(User u) {
		ArrayList<User> newList=new ArrayList<User>();
		
		for(int i=0;i<userList.size();i++)
			if(!(u.getId() == userList.get(i).getId() && u.getRole()==userList.get(i).getRole()))
			{
				newList.add(userList.get(i));				
			}
		
		userList=newList;
		
	}
	
}
package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.*;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUISecretary extends JFrame {

	private JPanel contentPane;
	private JTable patientTable;
	private JTable consultationTable;
	private JTextField idField;
	private JTextField nameField;
	private JTextField codeField;
	private JTextField birthField;
	private JTextField adrField;
	private JTextField idcField;
	private JTextField idpField;
	private JTextField iddField;
	private JTextField dateField;
	private JTextField infoField;

	public GUISecretary() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Secretary area");
		setBounds(100, 100, 700, 670);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 11, 561, 326);
		tabbedPane.setBounds(0, 11, 600, 620);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Patient", null, panel, null);
		panel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();		
		
		scrollPane.setBounds(10, 11, 536, 151);
		panel.add(scrollPane);

		patientTable = new JTable();
		scrollPane.setViewportView(patientTable);
		patientTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {				
				int row = patientTable.getSelectedRow();

				idField.setText(patientTable.getModel().getValueAt(row, 0).toString());
				nameField.setText(patientTable.getModel().getValueAt(row, 1).toString());
				codeField.setText(patientTable.getModel().getValueAt(row, 2).toString());
				birthField.setText(patientTable.getModel().getValueAt(row, 3).toString());
				adrField.setText(patientTable.getModel().getValueAt(row, 4).toString());
				
			}
		});
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(10, 445, 66, 14);
		panel.add(lblId);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 470, 66, 14);
		panel.add(lblName);
		
		JLabel lblCode = new JLabel("CNP");
		lblCode.setBounds(10, 495, 66, 14);
		panel.add(lblCode);
		
		JLabel lblBirth = new JLabel("Birthday");
		lblBirth.setBounds(10, 520, 66, 14);
		panel.add(lblBirth);
		
		JLabel lblAdr = new JLabel("Address");
		
		lblAdr.setBounds(10, 545, 66, 14);
		panel.add(lblAdr);
		
		idField = new JTextField();
		idField.setBounds(59, 445, 110, 20);
		panel.add(idField);
		idField.setColumns(10);
		
		nameField = new JTextField();
		nameField.setBounds(58, 470, 111, 20);
		panel.add(nameField);
		nameField.setColumns(10);
		
		codeField = new JTextField();
		codeField.setBounds(59, 495, 110, 20);
		panel.add(codeField);
		codeField.setColumns(10);
		
		birthField = new JTextField();
		birthField.setBounds(59, 520, 110, 20);
		panel.add(birthField);
		birthField.setColumns(10);
		
		adrField = new JTextField();
		adrField.setBounds(59, 545, 110, 20);
		panel.add(adrField);
		adrField.setColumns(10);
		
		JButton btnAdd = new JButton("Create"); 
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 9998); 
				Patient p=new Patient();
				p.setId(Integer.parseInt(idField.getText()));
				p.setName(nameField.getText());
				p.setCNP(Integer.parseInt(codeField.getText()));
				p.setBirth(birthField.getText());
				p.setAddress(adrField.getText());
				client.addPatient(p);
				try {
					client.connect();
					client.writeMessage("addPatient:"+idField.getText()+":"+nameField.getText()+":"
						+codeField.getText()+":"+birthField.getText()+":"+adrField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				init();
				
			}
		});
		btnAdd.setBounds(197, 545, 89, 23);
		panel.add(btnAdd);
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 9998); 
				Patient p=new Patient();
				p.setId(Integer.parseInt(idField.getText()));
				p.setName(nameField.getText());
				p.setCNP(Integer.parseInt(codeField.getText()));
				p.setBirth(birthField.getText());
				p.setAddress(adrField.getText());
				client.updatePatient(p);
				try {
					client.connect();
					client.writeMessage("updatePatient:"+idField.getText()+":"+nameField.getText()+":"
						+codeField.getText()+":"+birthField.getText()+":"+adrField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!" + e1.getMessage());
				}
				
				init();
			}
		});
		btnUpdate.setBounds(290, 545, 89, 23);
		panel.add(btnUpdate);
		
		JButton btnNotify = new JButton("Arrived");
		btnNotify.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 9998); 
				
				try {
					client.connect();
					client.writeMessage("arrived");
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!" + e1.getMessage());
				}
				
				
			}
		});
		btnNotify.setBounds(383, 545, 89, 23);
		panel.add(btnNotify);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Consultation", null, panel_1, null);
		panel_1.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 536, 128);
		panel_1.add(scrollPane_1);

		consultationTable = new JTable();
		consultationTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = consultationTable.getSelectedRow();

				idcField.setText(consultationTable.getModel().getValueAt(row, 0).toString());
				idpField.setText(consultationTable.getModel().getValueAt(row, 1).toString());
				iddField.setText(consultationTable.getModel().getValueAt(row, 2).toString());
				dateField.setText(consultationTable.getModel().getValueAt(row, 3).toString());
				infoField.setText(consultationTable.getModel().getValueAt(row, 4).toString());
			}
		});
		scrollPane_1.setViewportView(consultationTable);
		
		JLabel lblId_1 = new JLabel("ID");
		lblId_1.setBounds(10, 470, 66, 14);
		panel_1.add(lblId_1);
		
		JLabel lblPatient = new JLabel("Patient");
		lblPatient.setBounds(10, 495, 66, 14);
		panel_1.add(lblPatient);
		
		JLabel lblDoctor = new JLabel("Doctor");
		lblDoctor.setBounds(10, 520, 66, 14);
		panel_1.add(lblDoctor);
		
		JLabel lblSchedule = new JLabel("Date");
		lblSchedule.setBounds(10, 545, 66, 14);
		panel_1.add(lblSchedule);
		
		idcField = new JTextField();
		idcField.setBounds(59, 470, 110, 20);
		panel_1.add(idcField);
		idcField.setColumns(10);
		
		idpField = new JTextField();
		idpField.setBounds(59, 495, 110, 20);
		panel_1.add(idpField);
		idpField.setColumns(10);
		
		iddField = new JTextField();
		iddField.setBounds(59, 520, 110, 20);
		panel_1.add(iddField);
		iddField.setColumns(10);
		
		dateField = new JTextField();
		dateField.setBounds(59, 545, 110, 20);
		panel_1.add(dateField);
		dateField.setColumns(10);
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 9998); 
				Consultation c=new Consultation();
				c.setId(Integer.parseInt(idcField.getText()));
				c.setPatientId(Integer.parseInt(idpField.getText()));
				c.setDoctorId(Integer.parseInt(iddField.getText()));
				c.setDate(dateField.getText());
				
				try {
					client.connect();
					client.writeMessage("getConsultationList");
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!" + e1.getMessage());
				}
				
				if(client.checkAvailable(dateField.getText(), Integer.parseInt(iddField.getText())))
				{
					client.addConsultation(c);
					try {
						client.connect();
						client.writeMessage("addConsultation:"+idcField.getText()+":"+idpField.getText()+":"
							+iddField.getText()+":"+dateField.getText());
						client.readResponse();
					} catch (IOException e1) {
						System.err
								.println("Cannot establish connection. Server may not be up."
										+ e1.getMessage());
					}
				}
				else{
					JOptionPane.showMessageDialog(null, "Doctor not available");
				}
				
				init();
				
			}
		});
		btnCreate.setBounds(197, 545, 89, 23);
		panel_1.add(btnCreate);
		
		JButton btnUpdate_1 = new JButton("Update");
		btnUpdate_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
					Client client = new Client("localhost", 9998); 
					Consultation c=new Consultation();
					c.setId(Integer.parseInt(idcField.getText()));
					c.setPatientId(Integer.parseInt(idpField.getText()));
					c.setDoctorId(Integer.parseInt(iddField.getText()));
					c.setDate(dateField.getText());
					client.updateConsultation(c);
					try {
						client.connect();
						client.writeMessage("updateConsultation:"+idcField.getText()+":"+idpField.getText()+":"
								+iddField.getText()+":"+dateField.getText());
						client.readResponse();
					} catch (IOException e1) {
						System.err.println("Can't connect!" + e1.getMessage());
					}
					
					init();
				
			}
		});
		btnUpdate_1.setBounds(290, 545, 89, 23);
		panel_1.add(btnUpdate_1);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 9998); 
				client.deleteConsultation(Integer.parseInt(idcField.getText()));
				try {
					client.connect();
					client.writeMessage("deleteConsultation:"+idcField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!" + e1.getMessage());
				}
				
				init();
			}
		});
		btnDelete.setBounds(383, 545, 89, 23);
		panel_1.add(btnDelete);
		
		init();
	}
	
	private void init() {
		Client client = new Client("localhost", 9998);
		try {
			client.connect();
			client.writeMessage("getPatientList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!" + e.getMessage());
		}
		patientTable.setModel(ViewTable.patientsToTableModel(client.getPatients()));

		try {
			client.connect();
			client.writeMessage("getConsultationList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		consultationTable.setModel(ViewTable.consultationsToTableModel(client.getConsultations()));
	}
}

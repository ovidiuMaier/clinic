package gui;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.*;

import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.ButtonGroup;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUIAdmin extends JFrame {

	private JPanel contentPanel;
	private JTable userTable;
	private JTextField idField;
	private JTextField userField;
	private JTextField passField;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JComboBox<String> userComboBox;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	 
	public GUIAdmin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Admin area");
		setBounds(100, 100, 700, 670);
		
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 670, 400);
		contentPanel.add(scrollPane);
		
		String[] Strings = {"admin", "doctor", "secretary"};
		JComboBox userComboBox = new JComboBox(Strings);
		userComboBox.setBounds(86, 545, 86, 20);
		contentPanel.add(userComboBox);
		
		userTable = new JTable();
		userTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = userTable.getSelectedRow();

				idField.setText(userTable.getModel().getValueAt(row, 0).toString());
				userField.setText(userTable.getModel().getValueAt(row, 1).toString());
				passField.setText(userTable.getModel().getValueAt(row, 2).toString());	
				String type=userTable.getModel().getValueAt(row, 3).toString();
				
				System.out.println("tip:"+type+":");
				switch(type)
				{
				case "secretary":
					userComboBox.setSelectedItem("secretary");break;
				case "doctor":
					userComboBox.setSelectedItem("doctor");break;
				}
				
			}
		});
		scrollPane.setViewportView(userTable);
		
		JLabel labelId = new JLabel("ID");
		labelId.setBounds(10, 470, 46, 14);
		contentPanel.add(labelId);
		
		JLabel lblUser = new JLabel("Email");
		lblUser.setBounds(10, 495, 46, 14);
		contentPanel.add(lblUser);
		
		JLabel labelPassword = new JLabel("Password");
		labelPassword.setBounds(10, 520, 66, 14);
		contentPanel.add(labelPassword);
		
		JLabel labelRole = new JLabel("Role");
		labelRole.setBounds(10, 545, 66, 14);
		contentPanel.add(labelRole);
		
		
		idField = new JTextField();
		idField.setBounds(86, 470, 86, 20);
		contentPanel.add(idField);
		idField.setColumns(10);
		
		userField = new JTextField();
		userField.setBounds(86, 495, 86, 20);
		contentPanel.add(userField);
		userField.setColumns(10);
		
		passField = new JTextField();
		passField.setBounds(86, 520, 86, 20);
		contentPanel.add(passField);
		passField.setColumns(10);
		
		
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 9998); 
			
				User u=new User(userField.getText(),passField.getText());
				u.setId(Integer.parseInt(idField.getText()));
				if(userComboBox.getSelectedItem() == "secretary")
					{	
						u.setRole("secretary");
						try {
							client.connect();
							client.writeMessage("addSecretary:"+idField.getText()+":"
												+userField.getText()+":"+passField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.addUser(u);
					}
				else if(userComboBox.getSelectedItem() == "doctor")
					{
						u.setRole("doctor");
						try {
							client.connect();
							client.writeMessage("addDoctor:"+idField.getText()+":"
												+userField.getText()+":"+passField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.addUser(u);
					}
				else if(userComboBox.getSelectedItem() == "admin")
				{
					try {
						client.connect();
						client.writeMessage("addAdmin:"+idField.getText()+":"
											+userField.getText()+":"+passField.getText());
						client.readResponse();
					} catch (IOException e1) {
						System.err.println("Can't connect!"+ e1.getMessage());
					}
				}
									
				init();
			}
		});
		btnCreate.setBounds(197, 545, 89, 23);
		contentPanel.add(btnCreate);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 9998); 
				 
				User u=new User(userField.getText(),passField.getText());
				u.setId(Integer.parseInt(idField.getText()));
				if(userComboBox.getSelectedItem() == "secretary")
					{	
						u.setRole("secretary");
						try {
							client.connect();
							client.writeMessage("updateSecretary:"+idField.getText()+":"
												+userField.getText()+":"+passField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.updateUser(u);
					}
				else if(userComboBox.getSelectedItem() == "doctor")
					{
						u.setRole("doctor");
						try {
							client.connect();
							client.writeMessage("updateDoctor:"+idField.getText()+":"
												+userField.getText()+":"+passField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.updateUser(u);
					}
				
									
				init();
				
			}
		});
		btnUpdate.setBounds(290, 545, 89, 23);
		contentPanel.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 9998); 
				 
				User u=new User(userField.getText(),passField.getText());
				u.setId(Integer.parseInt(idField.getText()));
				if(userComboBox.getSelectedItem() == "secretary")
					{	
						u.setRole("secretary");
						try {
							client.connect();
							client.writeMessage("deleteSecretary:"+idField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.deleteUser(u);
					}
				else if(userComboBox.getSelectedItem() == "doctor")
					{
						u.setRole("doctor");
						try {
							client.connect();
							client.writeMessage("deleteDoctor:"+idField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.deleteUser(u);
					}
				
									
				init();
				
			}
		});
		btnDelete.setBounds(383, 545, 89, 23);
		contentPanel.add(btnDelete);
		
		init();
	}
	
	private void init() {
		Client client = new Client("localhost", 9998);
		try {
			client.connect();
			client.writeMessage("getUserList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!" + e.getMessage());
		}
		userTable.setModel(ViewTable.usersToTableModel(client.getUsers()));
	}
}

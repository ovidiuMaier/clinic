package dao;

import java.util.*;
import javax.swing.*;

import java.io.FileInputStream;
import java.sql.*;

import model.*;


public class AdminDao {

    private Connection myConn;
    private ResultSet result_set = null;
    
    public AdminDao() throws Exception {
		
		// get db properties
		Properties props = new Properties();
		props.load(new FileInputStream("clinic.properties"));
		
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		String dburl = props.getProperty("dburl");
		
		// connect to database
		myConn = DriverManager.getConnection(dburl, user, password);
		
		System.out.println("Admin DAO - DB connection successful to: " + dburl);
	}
    
    public boolean loginAdmin(User u) {
    	PreparedStatement myStmt = null;
        try {
        	myStmt = myConn.prepareStatement("select * from admin where email=? and password=?");
            myStmt.setString(1, u.getUsername());
            myStmt.setString(2, u.getPassword());
            
            result_set = myStmt.executeQuery();

            if (result_set.next()) {
            	result_set.close();
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Invalid credentials!");
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        return false;
    }
    
    public void addAdmin(String id, String user, String pass) {
		PreparedStatement myStmt = null;
	      try {    
	          myStmt = myConn.prepareStatement("insert into admin values(?,?,?)");
	          
	          myStmt.setInt(1,Integer.parseInt(id));
	          myStmt.setString(2, user);
	          myStmt.setString(3, pass);
	        
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	          System.out.println("createA " + e);

	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }		
	}
    
    public void addDoctor(String id, String user, String pass) {
		PreparedStatement myStmt = null;
	      try {     
	          myStmt = myConn.prepareStatement("insert into doctor values(?,?,?)");
	          
	          myStmt.setInt(1,Integer.parseInt(id));
	          myStmt.setString(2, user);
	          myStmt.setString(3, pass);
	        
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	          System.out.println("createD " + e);

	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }		
	}

    public void updateDoctor(String id, String user, String pass) {
		PreparedStatement myStmt = null;
	      try {     
	          
	          myStmt = myConn.prepareStatement("update doctor set email=? , password=?  where doctor_id=?");	          
	          
	          myStmt.setString(1, user);
	          myStmt.setString(2, pass);
	          myStmt.setInt(3,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	          System.out.println("upD " + e);

	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }			
	}
    
    public void deleteDoctor(String id) {
		 PreparedStatement myStmt = null;
	       try {     
	           myStmt = myConn.prepareStatement("delete from doctor where doctor_id=?");
	           myStmt.setInt(1,Integer.parseInt(id));
	           myStmt.executeUpdate();                   

	       } catch (Exception e) {
	           JOptionPane.showMessageDialog(null, e);
	           System.out.println("delD " + e);

	       } finally {
	           try {
	        	   DAOUtils.close(myStmt);
	           } catch (Exception e) {
	           }
	       }
	}
    
	public void addSecretary(String id, String user, String pass) {
		PreparedStatement myStmt = null;
      try {     
          myStmt = myConn.prepareStatement("insert into secretary values(?,?,?)");
          
          myStmt.setInt(1,Integer.parseInt(id));
          myStmt.setString(2, user);
          myStmt.setString(3, pass);
          
          myStmt.executeUpdate();

      } catch (Exception e) {
          JOptionPane.showMessageDialog(null, e);
          System.out.println("createS " + e);

      } finally {
          try {
        	  DAOUtils.close(myStmt);
          } catch (Exception e) {
          }
      }		
	}

	public void updateSecretary(String id, String user, String pass) {
		PreparedStatement myStmt = null;
	      try {     
	          myStmt = myConn.prepareStatement("update secretary set email=? , password=?  where secretary_id=?");	          
	          
	          myStmt.setString(1, user);
	          myStmt.setString(2, pass);
	          myStmt.setInt(3,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	          System.out.println("upA " + e);

	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }			
	}

	public void deleteSecretary(String id) {
		PreparedStatement myStmt = null;
	       try {     
	           myStmt = myConn.prepareStatement("delete from secretary where secretary_id=?");
	           myStmt.setInt(1,Integer.parseInt(id));
	           myStmt.executeUpdate();                   

	       } catch (Exception e) {
	           JOptionPane.showMessageDialog(null, e);
	           System.out.println("delS " + e);

	       } finally {
	           try {
	        	   DAOUtils.close(myStmt);
	           } catch (Exception e) {
	           }
	       }

		
	}
	
	public String getUserList() {
		String uList = "userList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from secretary");
			result_set = myStmt.executeQuery();

			while (result_set.next()) {
				uList = uList + "," + result_set.getInt("secretary_id") + "="
						+ result_set.getString("email") + "=" + result_set.getString("password")
						+ "=secretary";
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}	
			try {
				myStmt = myConn.prepareStatement("select * from doctor");
				result_set = myStmt.executeQuery();

				while (result_set.next()) {
					uList = uList + "," + result_set.getInt("doctor_id") + "="
							+ result_set.getString("email") + "=" + result_set.getString("password")
							+ "=doctor";
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);

			} finally {
				try {
					DAOUtils.close(myStmt, result_set);
				} catch (Exception e) {
				}
			}
		return uList;	
	}

}

package dao;

import model.User;
import javax.swing.JOptionPane;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Properties;
import java.sql.PreparedStatement;

public class DoctorDao {

	private Connection myConn;
    private ResultSet result_set = null;
    
    public DoctorDao() throws Exception {
		
		// get db properties
		Properties props = new Properties();
		props.load(new FileInputStream("clinic.properties"));
		
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		String dburl = props.getProperty("dburl");
		
		// connect to database
		myConn = DriverManager.getConnection(dburl, user, password);
		
		System.out.println("Doctor DAO - DB connection successful to: " + dburl);
	}

	public boolean loginDoctor(User u) {
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from doctor where email=? and password=?");
			myStmt.setString(1, u.getUsername());
			myStmt.setString(2, u.getPassword());
			result_set = myStmt.executeQuery();

			if (result_set.next()) {
				result_set.close();
				myStmt.close();
				return true;
			} else {
				JOptionPane.showMessageDialog(null, "Invalid credentials!");
				return false;
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}
		return false;
	}
}

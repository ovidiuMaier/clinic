package dao;

import java.util.Properties;

import java.io.FileInputStream;
import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.DriverManager;

import javax.swing.JOptionPane;

import model.*;

public class SecretaryDao {

	private Connection myConn;
    private ResultSet result_set = null;
    
    public SecretaryDao() throws Exception {
		
		// get db properties
		Properties props = new Properties();
		props.load(new FileInputStream("clinic.properties"));
		
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		String dburl = props.getProperty("dburl");
		
		// connect to database
		myConn = DriverManager.getConnection(dburl, user, password);
		
		System.out.println("Doctor DAO - DB connection successful to: " + dburl);
	}


	public boolean loginSecretary(User u) {
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from secretary where email=? and password=?");
			myStmt.setString(1, u.getUsername());
			myStmt.setString(2, u.getPassword());
			result_set = myStmt.executeQuery();
			if (result_set.next()) {
				result_set.close();
				myStmt.close();
				return true;
			} else {
				JOptionPane.showMessageDialog(null, "Invalid credentials!");
				return false;
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		}

		return false;
	}

	public String getPatientList() {
		String pList = "patientList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from patient");
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				pList = pList + "," + result_set.getInt("patient_id") + "="
						+ result_set.getString("name") + "=" + result_set.getInt("CNP")
						+ "=" + result_set.getString("birth") + "="
						+ result_set.getString("address");
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return pList;
	}

	public String getConsultationList() {
		String cList = "consultationList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from consultation");
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				cList = cList + "," + result_set.getInt("consultation_id") + "="
						+ result_set.getInt("patient_id") + "=" + result_set.getInt("doctor_id")
						+ "=" + result_set.getString("date");
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "getConsList " + e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return cList;
	}

	public void addPatient(String id, String name, String code,String birth, String adr) {
		PreparedStatement myStmt = null;
      try {     
          myStmt = myConn.prepareStatement("insert into patient values(?,?,?,?,?)");
          myStmt.setInt(1,Integer.parseInt(id));
          myStmt.setString(2, name);
          myStmt.setInt(3, Integer.parseInt(code));
          myStmt.setString(4, birth);
          myStmt.setString(5, adr);
          myStmt.executeUpdate();         

      } catch (Exception e) {
          JOptionPane.showMessageDialog(null, e);
      } finally {
          try {
        	  DAOUtils.close(myStmt);
          } catch (Exception e) {
          }
      }
		
	}
	
	public void updatePatient(String id, String name, String code,String birth, String adr) {
		PreparedStatement myStmt = null; 
	      try {     
	          myStmt = myConn.prepareStatement("update patient set name=? , CNP=? , birth=?, address=? where patient_id=?");
	          myStmt.setString(1, name);
	          myStmt.setInt(2, Integer.parseInt(code));
	          myStmt.setString(3, birth);
	          myStmt.setString(4, adr);
	          myStmt.setInt(5,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}

	public void addConsultation(String id, String idp, String idd,String date) {
		PreparedStatement myStmt = null;
	      try {     
	          myStmt = myConn.prepareStatement("insert into consultation values(?,?,?,?)");
	          myStmt.setInt(1,Integer.parseInt(id));
	          myStmt.setInt(2,Integer.parseInt(idp));
	          myStmt.setInt(3,Integer.parseInt(idd));
	          myStmt.setString(4, date);
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}

	public void updateConsultation(String id, String idp, String idd,String date) {
		 PreparedStatement myStmt = null;
		 try {     
	          myStmt = myConn.prepareStatement("update consultation set patient_id=? , doctor_id=? , date=? where consultation_id=?");
	          myStmt.setInt(1, Integer.parseInt(idp));
	          myStmt.setInt(2, Integer.parseInt(idd));
	          myStmt.setString(3, date);
	          myStmt.setInt(4,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}

	public void deleteConsultation(String id) {
		 PreparedStatement myStmt = null;
       try {     
           myStmt = myConn.prepareStatement("delete from consultation where consultation_id=?");
           myStmt.setInt(1,Integer.parseInt(id));
           myStmt.executeUpdate();                   
       } catch (Exception e) {
           JOptionPane.showMessageDialog(null, e);
       } finally {
           try {
        	   DAOUtils.close(myStmt);
           } catch (Exception e) {
           }
       }
	}

}

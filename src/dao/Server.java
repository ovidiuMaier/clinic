package dao;
import java.io.*;
import java.net.*;
import model.*;

public class Server implements Runnable {
	private Socket connection;
	private int ID;

	public Server(Socket connection, int ID) {
		this.connection = connection;
		this.ID = ID;
	}

	public void run() {
		try {
			readFromClient(connection);
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				connection.close();
			} catch (IOException e) {
			}
		}
	}

	public void readFromClient(Socket client) throws Exception {
		BufferedInputStream is = new BufferedInputStream(
				client.getInputStream());
		InputStreamReader reader = new InputStreamReader(is);
		StringBuffer process = new StringBuffer();
		int character;
		while ((character = reader.read()) != 13) {
			process.append((char) character);
		}
		System.out.println("readfromclient " + process);


		String returnCode = null,op, toBeParsed = process + "";
		String delims = ":";
		String[] tokens = toBeParsed.split(delims);
		op = tokens[0];
		
		SecretaryDao secretary_dao = new SecretaryDao();
		DoctorDao doctor_dao = new DoctorDao();
		AdminDao admin_dao = new AdminDao();
		
		switch (op) {
		case "secretaryLogin":
			if(secretary_dao.loginSecretary(new User(tokens[1], tokens[2])))
				returnCode="validSecretary"+ (char) 13;
			else 
				returnCode="invalidSecretary"+ (char) 13;			
			break;
		case "doctorLogin":
			if(doctor_dao.loginDoctor(new User(tokens[1], tokens[2])))
				returnCode="validDoctor"+ (char) 13;
			else 
				returnCode="invalidDoctor"+ (char) 13;			
			break;
		case "adminLogin":
			
			if(admin_dao.loginAdmin(new User(tokens[1], tokens[2])))
				returnCode="validAdmin"+ (char) 13;
			else 
				returnCode="invalidAdmin"+ (char) 13;			
			break;
		case "getPatientList":
			   returnCode= secretary_dao.getPatientList();
			break;
		case "getConsultationList":
			   returnCode= secretary_dao.getConsultationList();
			break;
		case "addPatient":
			secretary_dao.addPatient(tokens[1],tokens[2],tokens[3],tokens[4],tokens[5]);
			break;
		case "updatePatient":
			secretary_dao.updatePatient(tokens[1],tokens[2],tokens[3],tokens[4],tokens[5]);
			break;
		case "addConsultation":
			secretary_dao.addConsultation(tokens[1],tokens[2],tokens[3],tokens[4]);		
			break;
		case "updateConsultation":
			secretary_dao.updateConsultation(tokens[1],tokens[2],tokens[3],tokens[4]);		
			break;
		case "deleteConsultation":
			secretary_dao.deleteConsultation(tokens[1]);		
			break;
		case "getUserList":
			   returnCode= admin_dao.getUserList();
			break;
		case "addSecretary":
			admin_dao.addSecretary(tokens[1],tokens[2],tokens[3]);		
			break;
		case "addDoctor":
			admin_dao.addDoctor(tokens[1],tokens[2],tokens[3]);		
			break;
		case "addAdmin":
			admin_dao.addAdmin(tokens[1],tokens[2],tokens[3]);		
			break;
		case "updateSecretary":
			admin_dao.updateSecretary(tokens[1],tokens[2],tokens[3]);		
			break;
		case "updateDoctor":
			admin_dao.updateDoctor(tokens[1],tokens[2],tokens[3]);		
			break;	
		case "deleteSecretary":
			admin_dao.deleteSecretary(tokens[1]);		
			break;	
		case "deleteDoctor":
			admin_dao.deleteDoctor(tokens[1]);		
			break;
		case "arrived":
			returnCode= "patientArrived";
			break;	
		}
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
		}
		sendMessage(client, returnCode);
	}

	private void sendMessage(Socket client, String message) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				client.getOutputStream()));
		writer.write(message);
		writer.flush();
	}

	public static void main(String[] args) {
		int port = 9998;
		int number = 0;
		System.out.println("Server started at port " + port);

		try {
			ServerSocket serverSocket = new ServerSocket(port);
			System.out.println("Server Initialized");
		
			while (true) {
				Socket client = serverSocket.accept();
				Runnable runnable = new Server(client, ++number);
				Thread thread = new Thread(runnable);
				thread.start();
			}
		} catch (Exception e) {
		}
	}
}